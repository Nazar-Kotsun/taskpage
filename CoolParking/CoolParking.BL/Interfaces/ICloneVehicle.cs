﻿using System;

namespace CoolParking.BL.Interfaces
{
    public interface ICloneVehicle<Vehicle>
    {
        Vehicle Clone();
    }
}
