﻿// TODO: implement class TimerService from the ITimerService interface.
//       Service have to be just wrapper on System Timers.


using System.Timers;
using CoolParking.BL.Interfaces;


namespace CoolParking.BL.Services
{

    public class TimerService : ITimerService
    {
        private Timer Timer;

        public TimerService()
        {
            Timer = new Timer();
         
        }

        public double Interval
        {
            get
            {
                return Timer.Interval;
            }

            set
            {
                Timer.Interval = value;
            }
        }

        public event ElapsedEventHandler Elapsed
        {
            add
            {
                this.Timer.Elapsed += value;
            }
            remove
            {
                this.Timer.Elapsed -= value;
            }
        }

        public void Start()
        {
            Timer.Start();
        }

        public void Stop()
        {
            Timer.Stop();
        }

        public void Dispose()
        {
            Timer.Stop();
            Timer.Dispose();
        }

    }
}
