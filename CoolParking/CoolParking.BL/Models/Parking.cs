﻿// TODO: implement class Parking.
//       Implementation details are up to you, they just have to meet the requirements 
//       of the home task and be consistent with other classes and tests.

using System;
using System.Collections.Generic;


namespace CoolParking.BL.Models
{
    public class Parking 
    {

        private static readonly Parking ParkingInstance = new Parking();
        public static List<Vehicle> Vehicles { get; private set; }

        public static decimal Balance { get; set; }

        private Parking()
        {
            Vehicles = new List<Vehicle>();
        }
        public static Parking GetInstanceParking()
        {
            return ParkingInstance;
        }

    }
}