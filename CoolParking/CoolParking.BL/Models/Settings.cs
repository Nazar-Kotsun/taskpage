﻿// TODO: implement class Settings.
//       Implementation details are up to you, they just have to meet the requirements of the home task.

namespace CoolParking.BL.Models
{
    public static class Settings
    {
        public static decimal StartBalance = 0;
        public static int Capacity = 10;

        public static int PeriodWithdrawSeconds = 5;
        public static int PeriodLogSeconds = 60;

        public static decimal TariffPassengerCar = 2;
        public static decimal TariffTruck = 5;
        public static decimal TariffBus = 3.5M;
        public static decimal TariffMotorcycle = 1;

        public static decimal CoefficientFine = 2.5M;
    }

}
