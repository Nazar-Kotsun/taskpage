using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using CoolParking.BL.Models;

namespace CoolParking.WebAPI
{
    public class VehicleService
    {
        [Required]
        [JsonPropertyName("id")]
        public string Id { get; set; }
        
        [JsonPropertyName("vehicleType")]
        [Required]
        public VehicleType VehicleType { get; set; }
        
        [Required]
        [JsonPropertyName("balance")]
        public decimal Balance { get; set; }

        public VehicleService()
        {
            
        }
        
    }
}