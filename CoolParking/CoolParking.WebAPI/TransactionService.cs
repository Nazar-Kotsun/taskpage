using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace CoolParking.WebAPI
{
    public class TransactionService
    {
        [Required]
        [JsonPropertyName("id")]
        public string Id { get; set; }
        
        [Required]
        [JsonPropertyName("sum")]
        public decimal Sum { get; set; }
    }
}