﻿using System.Timers;

using CoolParking.BL.Interfaces;


namespace CoolParking.BL.Tests
{
    public class FakeTimerService :  ITimerService
    {
        private Timer Timer;

        public FakeTimerService()
        {
            Timer = new Timer();
        }

        public double Interval { get; set; }

        public event ElapsedEventHandler Elapsed;

        public void FireElapsedEvent()
        {
            //
            Elapsed?.Invoke(this, null);
        }

        public void Start()
        {
            Timer.Start();
        }

        public void Stop()
        {
            Timer.Stop();
        }

        public void Dispose()
        {
            Timer.Dispose();
        }

    }
}
